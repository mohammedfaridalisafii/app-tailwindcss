module.exports = {
  // menginisialisasi letak folder/file yang nantinya akan menggunakan library tailwind 
  // content: ['./src/**/*.{html,js}'],
  // ./ -> route
  // src -> folder name ex src
  //  ** -> read all folder on folder
  // * -> read all file with extension html and js  
  
  content: ['./**/*.{html,js}'],
  theme: {
    extend: {},
  },
  plugins: [],
}
